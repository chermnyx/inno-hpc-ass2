#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <omp.h>

#define REPORT_DATA "report_data.txt"
static std::ofstream report_data;
#define THREADS 16

#include "t1.cpp"
#include "t2.cpp"


void run_t2() {
    auto dim = std::vector<int>{10, 50, 100, 500, 1000};
    auto acc = std::vector<double>{0.001, 0.0001, 0.00001};

#pragma omp parallel default(none) shared(dim, acc, report_data) num_threads(THREADS)
    {

#pragma omp for ordered schedule(dynamic) collapse(2)
        for (auto d: dim) {
            for (auto a: acc) {
                t2::t2_main(d, a, false);
            }
        }
    }

    t2::t2_main(500, 0.00001, true);
}

void run_t1() {
    double t;
    // for N=100 L=20 the method diverges
    // t1::solve(100, 20, t);
    t1::solve(100, 1000, t);


    auto nt = std::vector<int>{1, 2, 4, 8, 16};
    for (auto t: nt) {
        std::ofstream th_graph;
        th_graph.open("t1.T" + std::to_string(t) + ".txt");


        auto L = 25000;
        for (auto N = 2500; N <= 10000; N += 250) {
            std::cout << "t: " << t << ", N: " << N << std::endl;
            double time;
            t1::solve_omp(N, L, time, t);
            th_graph << N << " " << time << "\n";
        }

        th_graph.close();
    }
}

int main() {
    std::cout << "Dmitrii Chermnykh, d.chermnykh@innopolis.university, PA-II (p02)\n";
    report_data.open(REPORT_DATA);
    run_t1();
    run_t2();

    return 0;
}