#pragma once

#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 200

#include <ext/stdio_filebuf.h>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <sstream>
#include <fstream>

#define platform_ind 0
#define GNUPLOT_COMMAND "gnuplot -persist"

std::string read_path(const std::string &path) {
    auto s = std::ostringstream();
    std::ifstream input_file(path);
    if (!input_file.is_open()) {
        throw std::runtime_error("Unable to open " + path);
    }
    s << input_file.rdbuf();
    return s.str();
}

std::ostream* get_gnuplot() {
    FILE *gnuplotfd = popen(GNUPLOT_COMMAND, "w");
    if (gnuplotfd == nullptr)
        throw std::runtime_error("UNABLE TO START GNUPLOT");
    auto filebuf = new __gnu_cxx::stdio_filebuf<char>(gnuplotfd, std::ios::out);
    auto plot = new std::ostream(filebuf);
    return plot;
}
