#pragma once

#include <iostream>
#include <fstream>
#include <omp.h>
#include <vector>
#include <memory>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cstring>

namespace t1 {
    const auto X_L = 0.0;
    const auto X_R = 1.0;
    const auto T_L = 0.0;
    const auto T_R = 2.0;

    double f(double x) {
        return std::exp(std::cos(x)) / 10;
    }

    std::vector<std::vector<double>> solve(size_t N, size_t L, double &time) {
        auto start = omp_get_wtime();

        std::vector<std::vector<double>> solution;


        auto dx = double(X_R - X_L) / N;
        auto dt = double(T_R - T_L) / L;

        for (auto x = 0; x < N; ++x) {
            solution.emplace_back();
            for (auto t = 0; t < L; ++t) {
                solution[x].emplace_back();
                if (t == 0) solution[x][t] = 0.1 * std::sin(std::numbers::pi * x * dx);
                else solution[x][t] = 0;  // for t >= 1
            }
        }

        for (auto x = 0; x < N; ++x) {
            // v_i^1 = u_0(x) * t + v_i^0 = 0 + v_i^0
            solution[x][1] = solution[x][0];
        }

        for (auto t = 1; t < L - 1; ++t) {
            for (auto x = 1; x < N - 1; ++x) {
                solution[x][t + 1] = 2 * solution[x][t] - solution[x][t - 1]
                                     + (dt / dx) * (dt / dx) * f(x * dx) *
                                       (solution[x + 1][t] - 2 * solution[x][t] + solution[x - 1][t]);
            }
        }

        time = omp_get_wtime() - start;

        std::ofstream plot_data;
        plot_data.open("t1.gif.txt");

        for (auto t = 0; t < L; ++t) {
            for (auto x = 0; x < N; ++x) {
                plot_data << x * dx << " " << solution[x][t] << "\n";
            }

            plot_data << "\n\n";
        }

        return solution;
    }

    std::vector<std::vector<double>> solve_omp(size_t N, size_t L, double &time, size_t nthreads) {
        auto start = omp_get_wtime();
        std::vector<std::vector<double>> solution;
        auto dx = double(X_R - X_L) / N;
        auto dt = double(T_R - T_L) / L;

        for (auto x = 0; x < N; ++x) {
            solution.emplace_back();
            for (auto t = 0; t < L; ++t) {
                solution[x].emplace_back();
                if (t == 0) solution[x][t] = 0.1 * std::sin(std::numbers::pi * x * dx);
                else solution[x][t] = 0;  // for t >= 1
            }
        }
#pragma omp parallel default(none)  shared(N, L, X_R, X_L, T_R, T_L, solution, dt, dx) num_threads(nthreads)
        {
#pragma omp for
            for (auto x = 0; x < N; ++x) {
                // v_i^1 = u_0(x) * t + v_i^0 = 0 + v_i^0
                solution[x][1] = solution[x][0];
            }

            for (auto t = 1; t < L - 1; ++t) {
                // t is fixed so we can parallelize for variable x
#pragma omp for
                for (auto x = 1; x < N - 1; ++x) {
                    solution[x][t + 1] = 2 * solution[x][t] - solution[x][t - 1]
                                         + (dt / dx) * (dt / dx) * f(x * dx) *
                                           (solution[x + 1][t] - 2 * solution[x][t] + solution[x - 1][t]);
                }
            }
        }
        time = omp_get_wtime() - start;
        return solution;
    }
}