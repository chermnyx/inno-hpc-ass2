# HPC PA-II (p02)

Dmitrii Chermnykh, <d.chermnykh@innopolis.university>

## Task 1

### Gif animation

The method used to solve the equation does not converge for given 20 time steps and 100 spacial steps.

Let's solve it for 1000 time steps and 100 spacial steps:

![t1 gif](./t1.gif)

### Parallelizing

Number of steps varies from 2500 to 10000 with step 250. The system has 8 physical cores.

![Result](./t1.png)

Note: there is no improvement in speed for 16 threads compared to 8 threads because host system has only 8 physical cores.

## Task 2

### Heatmap of the solution (for n=500, ϵ=0.00001)

![heatmap](./t2.png)

### Comparison for different dimensions and accuracy

| ϵ \ n=m | 10                                         | 50                                          | 100                                          | 500                                           | 1000                                                                  |
| ------- | ------------------------------------------ | ------------------------------------------- | -------------------------------------------- | --------------------------------------------- | --------------------------------------------------------------------- |
| 0.001   | StepCount: 22 <br> AcAccuracy: 0.000465348 | StepCount: 392 <br> AcAccuracy: 0.000997954 | StepCount: 1156 <br> AcAccuracy: 0.000998721 | StepCount: 4497 <br> AcAccuracy: 0.000999992  | StepCount: 4453 <br> AcAccuracy: 0.000999951                          |
| 0.0001  | StepCount: 25 <br> AcAccuracy: 9.89389e-05 | StepCount: 560 <br> AcAccuracy: 9.89606e-05 | StepCount: 1832 <br> AcAccuracy: 9.97001e-05 | StepCount: 22006 <br> AcAccuracy: 9.99876e-05 | StepCount: 47092 <br> AcAccuracy: 9.99991e-05                         |
| 0.00001 | StepCount: 28 <br> AcAccuracy: 9.67679e-06 | StepCount: 727 <br> AcAccuracy: 9.94818e-06 | StepCount: 2507 <br> AcAccuracy: 9.98344e-06 | StepCount: 38938 <br> AcAccuracy: 9.99883e-06 | StepCount: 50000 <br> AcAccuracy: 9.06794e-05 <br> _did not converge_ |
