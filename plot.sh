#!/usr/bin/env bash

gnuplot -persistent <<EOF
set term png size 512
set output "t1.png"

set xlabel "N"
set ylabel "Computational time"
set title "Task 1"

thnums = "1 2 4 8 16"

plot for [t in thnums] "t1.T".t.".txt" using 1:2 with lines title "nthreads: ".t
EOF

gnuplot -persistent <<EOF
set term gif animate optimize delay 1 size 512
set output "t1.gif"
set size square
set xlabel "x"
set ylabel "y"
set title "Task 1"

stats "t1.gif.txt" name "T1"
set xrange[T1_min_x:T1_max_x]
set yrange[T1_min_y:T1_max_y]

do for [i=0:T1_blocks-1] {
  plot "t1.gif.txt" index i u 1:2 w lines
}
EOF

gnuplot -persistent <<EOF
set term png size 512
set output "t2.png"
set xlabel "x"
set ylabel "y"
set title "Task 2"

set size ratio -1

set pm3d map interpolate 0,0
splot "t2.plot.txt" u 1:2:3
EOF
