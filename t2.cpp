#pragma once

#include <iostream>
#include <fstream>


#include <cstring>
#include <omp.h>

#include <vector>
#include <cmath>

namespace t2 {
#define N_MAX 50000

    double f(double x, double y) {
        return 4;
    };

    double mu1(double y) {
        return 1 + y * y;
    };

    double mu2(double y) {
        return 4 + y * y;
    };

    double mu3(double x) {
        return x * x + 4;
    };

    double mu4(double x) {
        return x * x + 9;
    };

    double FuncU(double x, double y) {
        return x * x + y + y;
    };

// Vector allocation
    template<typename T>
    void AllocateVector(T **Vector, int size) {
        (*Vector) = new T[size];
    }

// Vector release
    template<typename T>
    void FreeVector(T **Vector) {
        delete[](*Vector);
    }

// Vector printing
    void PrintVector(char *VectorName, double *Vector, int size) {
        printf("%s\n", VectorName);
        for (int i = 0; i < size; i++)
            printf("%lf ", Vector[i]);
        printf("\n");
    }

// x ∈ [1, 2]; y ∈ [2, 3]
    const auto X_L = 1.0;
    const auto X_R = 2.0;
    const auto Y_L = 2.0;
    const auto Y_R = 3.0;

    const auto LEFT_BOUND = (X_R - X_L);
    const auto RIGHT_BOUND = (Y_R - Y_L);

//--------------------------------------------------------------------------
// Initialization of the data by the partial differential equation
//--------------------------------------------------------------------------
// matrix allocation (n, m)
// Diagonals of the matrix are stored by rows
// the shift if the diagonal elements is stored in the index array
    void CreateDUMatrix(int n, int m, double **Matrix, int **Index) {
        double hsqr = (double) n * n / LEFT_BOUND / LEFT_BOUND; // 1/h
        double ksqr = (double) m * m / RIGHT_BOUND / RIGHT_BOUND; // 1/k
        double A = 2 * (hsqr + ksqr);
        int size = (n - 1) * (m - 1), bandWidth = 5;
        AllocateVector(Matrix, size * bandWidth);
        AllocateVector(Index, bandWidth);
        (*Index)[0] = -n + 1;
        (*Index)[1] = -1;
        (*Index)[2] = 0;
        (*Index)[3] = 1;
        (*Index)[4] = n - 1;
        for (int i = 0; i < size; i++) {
            if (i >= n - 1) (*Matrix)[i * bandWidth] = -ksqr;
            else (*Matrix)[i * bandWidth] = 0.0;
            if (i % (n - 1) != 0) (*Matrix)[i * bandWidth + 1] = -hsqr;
            else (*Matrix)[i * bandWidth + 1] = 0.0;
            (*Matrix)[i * bandWidth + 2] = A;
            if ((i + 1) % (n - 1) != 0) (*Matrix)[i * bandWidth + 3] = -hsqr;
            else (*Matrix)[i * bandWidth + 3] = 0.0;
            if (i < (n - 1) * (m - 2)) (*Matrix)[i * bandWidth + 4] = -ksqr;
            else (*Matrix)[i * bandWidth + 4] = 0.0;
        }
    }

// The right-hand side of the equation
    void CreateDUVector(int n, int m, double **Vector) {
        double h = LEFT_BOUND / (double) n;
        double k = RIGHT_BOUND / (double) m;
        double hsqr = (double) n * n / LEFT_BOUND / LEFT_BOUND;
        double ksqr = (double) m * m / RIGHT_BOUND / RIGHT_BOUND;
        AllocateVector(Vector, (n - 1) * (m - 1));
        for (int j = 0; j < m - 1; j++) {
            for (int i = 0; i < n - 1; i++)
                (*Vector)[j * (n - 1) + i] = f((double) (i + 1) * h, (double) (j + 1) * k);
            (*Vector)[j * (n - 1)] += hsqr * mu1(Y_L + (double) (j + 1) * k);
            (*Vector)[j * (n - 1) + n - 2] += hsqr * mu2(Y_L + (double) (j + 1) * k);
        }
        for (int i = 0; i < n - 1; i++) {
            (*Vector)[i] += ksqr * mu3(X_L + (double) (i + 1) * h);
            (*Vector)[(m - 2) * (n - 1) + i] += ksqr * mu4(X_L + (double) (i + 1) * h);
        }
    }

    void GetFirstApproximation(double *Result, int size) {
        for (int i = 0; i < size; i++)
            Result[i] = 0.0;
    }

    double GetWParam(double Step) {
        return 1.55;
    }

    double BandOverRelaxation(double *Matrix, double *Vector, double *Result, int *Index, int
    size, int bandWidth,
                              double WParam, double Accuracy, int &StepCount) {
        double CurrError;//achieved accuracy
        double sum, TempError;
        int ii, index = Index[bandWidth - 1], bandHalf = (bandWidth - 1) / 2;
        StepCount = 0;
        do {
            CurrError = -1.0;
            for (int i = index; i < size + index; i++) {
                ii = i - index;
                TempError = Result[i];
                sum = 0.0;
                for (int j = 0; j < bandWidth; j++)
                    sum += Matrix[ii * bandWidth + j] * Result[i + Index[j]];
                Result[i] = (Vector[ii] - sum) * WParam / Matrix[ii * bandWidth + bandHalf] + Result[i];
                TempError = fabs(Result[i] - TempError);
                if (TempError > CurrError) CurrError = TempError;
            }
            StepCount++;
        } while ((CurrError > Accuracy) && (StepCount < N_MAX));
        return CurrError;
    }

    double SolvePoisson(int n, int m, double *Solution, double Accuracy, double &ORAccuracy,
                        int &StepCount) {
        double *Matrix, *Vector, *Result;
        int *Index;
        int size = (n - 1) * (m - 1), ResSize = size + 2 * (n - 1), bandWidth = 5;
        double start, finish;
        double time;
        double WParam, step = (n / LEFT_BOUND > m / RIGHT_BOUND) ?
                              (double) LEFT_BOUND / n : (double) RIGHT_BOUND / m;
        CreateDUMatrix(n, m, &Matrix, &Index);
        CreateDUVector(n, m, &Vector);
        AllocateVector(&Result, ResSize);
        GetFirstApproximation(Result, ResSize);
        WParam = GetWParam(step);
        start = omp_get_wtime();
        ORAccuracy = BandOverRelaxation(Matrix, Vector, Result, Index, size, bandWidth,
                                        WParam, Accuracy, StepCount);
        finish = omp_get_wtime();
        time = (finish - start);
        memcpy(Solution, Result + n - 1, sizeof(double) * size);
        FreeVector(&Matrix);
        FreeVector(&Index);
        FreeVector(&Vector);
        FreeVector(&Result);
        return time;
    }

    double SolutionCheck(double *solution, int n, int m) {
        double h = LEFT_BOUND / (double) n, k = RIGHT_BOUND / (double) m;
        double err = 0, temp;
        for (int j = 0; j < m - 1; j++)
            for (int i = 0; i < n - 1; i++) {
                temp = fabs(solution[j * (n - 1) + i] - FuncU(X_L + (double) (i + 1) * h, Y_L + (double) (j + 1) * k));
                if (temp > err)
                    err = temp;
            }
        return err;
    }

    int t2_main(int n, double EPSILON, bool plot) {
        int m = n;
        int StepCount;
        int size;
        double time;
        double Accuracy = EPSILON;
        double AcAccuracy;
        double Correctness;
        double *Solution;
        size = (n - 1) * (m - 1);
        AllocateVector(&Solution, size);
        time = SolvePoisson(n, m, Solution, Accuracy, AcAccuracy, StepCount);

//    std::cout << SolutionCheck(Solution, n, m) << std::endl;

#pragma omp ordered
        {
            if (!plot) {
                std::cout << "Running t2 " << n << " " << EPSILON << std::endl;
                report_data << "n: " << n << "\n"
                            << "EPS: " << EPSILON << "\n"
                            << "StepCount: " << StepCount << "\n"
                            << "AcAccuracy: " << AcAccuracy << "\n"
                            << std::endl;
            }

            if (plot) {
                std::ofstream plot_data;
                plot_data.open("t2.plot.txt");

                for (auto i = 0; i < n - 1; ++i) {
                    for (auto j = 0; j < n - 1; ++j) {
                        plot_data << X_L + double(j) * LEFT_BOUND / n
                                  << " "
                                  << Y_L + double(i) * RIGHT_BOUND / n
                                  << " "
                                  << Solution[i * (n - 1) + j]
                                  << "\n";
                    }
                    plot_data << std::endl;
                }
            }
        }

        FreeVector(&Solution);
        return 0;
    }

}